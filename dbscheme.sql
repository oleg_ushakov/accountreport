CREATE SCHEMA accountdata;

CREATE TABLE accountdata.account
(
  id        INT PRIMARY KEY auto_increment NOT NULL,
  number    TEXT          NULL,
  balance   BIGINT(20)    NULL,
  timestamp DATETIME      NULL
)
  ENGINE = innodb
  DEFAULT CHARSET = utf8;

ALTER TABLE accountdata.account ADD CONSTRAINT account_id_uindex UNIQUE (id);


CREATE TABLE accountdata.transaction_record
(
  id       INT PRIMARY KEY auto_increment NOT NULL,
  from_account TEXT          NULL,
  to_account   TEXT          NULL,
  amount       BIGINT(20)    NULL,
  timestamp DATETIME      NULL
)
  ENGINE = innodb
  DEFAULT CHARSET = utf8;

ALTER TABLE accountdata.transaction_record ADD CONSTRAINT transaction_id_uindex
UNIQUE (id);
