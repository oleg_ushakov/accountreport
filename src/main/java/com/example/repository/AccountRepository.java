package com.example.repository;

import com.example.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by ThinkPad
 * @since 19.05.2017.
 */
public interface AccountRepository extends JpaRepository<AccountEntity, Long> {
    AccountEntity findByAccounNumberEquals(String accountNumber);
}
