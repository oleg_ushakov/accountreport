package com.example.repository;

import com.example.entity.TransactionRecordEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TransactionRecordRepository extends JpaRepository<TransactionRecordEntity, Long> {
//    Page<TransactionRecordEntity> findAllByFromAccountNumberOrToAccountNumber(String fromNumber, String toNumber, Pageable pageRequest);
    List<TransactionRecordEntity> findAllByFromAccountNumberOrToAccountNumber(String fromNumber, String toNumber);
}
