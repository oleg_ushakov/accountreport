package com.example.entity;

import com.example.entity.base.EntityAbstract;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by ThinkPad
 * since 19.05.2017.
 */
@Entity
@Table(name = "transaction_record")
public class TransactionRecordEntity extends EntityAbstract{
    @Getter  @Setter  @Column(name = "from_account")
    private String fromAccountNumber;

    @Getter  @Setter  @Column(name = "to_account")
    private String toAccountNumber;

    @Getter  @Setter  @Column(name = "amount")
    private Long amount;

    public TransactionRecordEntity(){
    }

    public TransactionRecordEntity(String fromAccountNumber, String toAccountNumber, Long amount) {
        this.fromAccountNumber = fromAccountNumber;
        this.toAccountNumber = toAccountNumber;
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TransactionRecordEntity that = (TransactionRecordEntity) o;

        if (!fromAccountNumber.equals(that.fromAccountNumber)) return false;
        if (!toAccountNumber.equals(that.toAccountNumber)) return false;
        return amount.equals(that.amount);
    }

    @Override
    public int hashCode() {
        int result = fromAccountNumber.hashCode();
        result = 31 * result + toAccountNumber.hashCode();
        result = 31 * result + amount.hashCode();
        return result;
    }

}