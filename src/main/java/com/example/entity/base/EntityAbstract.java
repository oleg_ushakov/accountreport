package com.example.entity.base;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public class EntityAbstract {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	@JsonIgnore
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="timestamp")
	@JsonIgnore
	private Date timestampdate;

	@PreUpdate
	void onPreUpdate() {
		this.setTimestampdate(new Date());
	}

	@PrePersist
	void onPreCreate() {
		this.setTimestampdate(new Date());
	}

	public void setTimestampdate(Date modifyDate) {
		this.timestampdate = modifyDate;
	}

	public Date getTimestampdate() {
		return timestampdate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

}