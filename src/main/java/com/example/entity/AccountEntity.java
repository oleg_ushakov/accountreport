package com.example.entity;

import com.example.entity.base.EntityAbstract;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by ThinkPad
 * since 19.05.2017.
 */
@Entity
@Table(name = "account")
public class AccountEntity  extends EntityAbstract{
    @Getter  @Setter @Column(name="number")
    private String accounNumber;

    @Getter  @Setter @Column(name="balance")
    private Long balance;

    public AccountEntity() {
    }

    public AccountEntity(String accounNumber, Long balance) {
        this.accounNumber = accounNumber;
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountEntity that = (AccountEntity) o;

        if (!accounNumber.equals(that.accounNumber)) return false;
        return balance.equals(that.balance);
    }

//    @Override
//    public int hashCode() {
//        int result = accounNumber.hashCode();
//        result = 31 * result + balance.hashCode();
//        return result;
//    }
}

