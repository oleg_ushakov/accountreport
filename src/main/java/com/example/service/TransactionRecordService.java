package com.example.service;

import com.example.entity.AccountEntity;
import com.example.entity.TransactionRecordEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

/**
 * Created by ThinkPad
 *
 * @since 22.05.2017.
 */
public interface TransactionRecordService {
    TransactionRecordEntity addTransactionRecord(String fromAccount, String toAccount, Long amount);
    List<TransactionRecordEntity> getTransactionByAccount(String accountNumber);
//    Page<TransactionRecordEntity> getTransactionByAccount(String accountNumber, Integer pageNumber);
}
