package com.example.service.impl;

import com.example.entity.AccountEntity;
import com.example.entity.TransactionRecordEntity;
import com.example.repository.TransactionRecordRepository;
import com.example.service.TransactionRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by ThinkPad
 * @since 19.05.2017.
 */

@Transactional
@Service
public class TransactionRecordServiceImpl implements TransactionRecordService {
    private static final int PAGE_SIZE = 10;

    @Autowired
    TransactionRecordRepository transactionRecordRepository;

    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public TransactionRecordEntity addTransactionRecord(String fromAccountNumber, String toAccountNumber, Long amount) {
        return transactionRecordRepository.save(
                new TransactionRecordEntity (fromAccountNumber, toAccountNumber, amount)
        );
    }

    @Transactional(readOnly = true)
    public List<TransactionRecordEntity> getTransactionByAccount(String accountNumber){
        return transactionRecordRepository.findAllByFromAccountNumberOrToAccountNumber(accountNumber, accountNumber);
    }

//    @Transactional(readOnly = true)
//    public Page<TransactionRecordEntity> getTransactionByAccount(String accountNumber, Integer pageNumber){
//        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "timestampdate");
//        return transactionRecordRepository.findAllByFromAccountNumberOrToAccountNumber(accountNumber, accountNumber, request);
//    }
}
