package com.example.service.impl;

import com.example.entity.AccountEntity;
import com.example.entity.TransactionRecordEntity;
import com.example.exceptions.AccountException;
import com.example.exceptions.EmptyValueException;
import com.example.exceptions.TransferException;
import com.example.repository.AccountRepository;
import com.example.service.AccountService;
import com.example.service.TransactionRecordService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by ThinkPad
 *
 * @since 19.05.2017.
 */
@Transactional
@Service
public class AccountServiceImpl implements AccountService {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    TransactionRecordService transactionRecordService;

    public AccountEntity createAccount(String accountNumber){
        if (accountNumber == null || accountNumber.equals("")){
            throw new EmptyValueException("Нужен номер счета!");
        }
        if (findByNumber(accountNumber) != null) {
            throw new AccountException("Счёт номер " + accountNumber + " уже существует");
        }
        return accountRepository.save(new AccountEntity(accountNumber, (long) 0));
    }

    @Transactional(readOnly = true)
    public AccountEntity findByNumber(@NonNull String accountNumber){
        return  accountRepository.findByAccounNumberEquals(accountNumber);
    }
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE)
    public TransactionRecordEntity transferMoney(@NonNull String fromAccountNumber,
                                                 @NonNull String toAccountNumber,
                                                 @NonNull Long amount) {
        if(findByNumber(toAccountNumber) == null) {
            throw new AccountException("Счёт № " + toAccountNumber + " введён неверно");
        }

        if (!fromAccountNumber.equals("0")) {  //очень условно для демо приложения позволяем переводить со счёта №"0" для пополнения баланса
            AccountEntity fromAccount = findByNumber(fromAccountNumber);

            if (fromAccount.getBalance() == null || fromAccount.getBalance() < amount) {
                throw new TransferException("Недостаточный баланс. Ваш текущий баланс "
                        + fromAccount.getBalance() + ". Запрошено к переводу " + amount);
            }
            if (amount == null || amount <= 0 ) {
                throw new TransferException("Неверное значение для перевода: " + amount);
            }
            withdrawMoney(fromAccountNumber, amount);
        }

        addMoney(toAccountNumber, amount);
        return transactionRecordService.addTransactionRecord(fromAccountNumber, toAccountNumber, amount);
    }

    @Transactional(readOnly = true)
    public List<TransactionRecordEntity> getAccountHistory(String accountNumber){
        List<TransactionRecordEntity> transactions = transactionRecordService.getTransactionByAccount(accountNumber);
        for(TransactionRecordEntity each:transactions) {
            if (each.getFromAccountNumber().equals(accountNumber)){
                each.setAmount(each.getAmount()*(-1));
            }
        }
        return transactions;
    }


    @Transactional(readOnly = true)
    public Iterable<AccountEntity> getAllAccountList(){
        return accountRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Iterable<AccountEntity> getAllAccountListByPage(){
        return accountRepository.findAll();
    }

    public AccountEntity setBalance(@NonNull String accountNumber, @NonNull Long amount){
        AccountEntity accountEntity = findByNumber(accountNumber);
        if( accountEntity != null) {
            accountEntity.setBalance(amount);
        }
        return accountEntity;
    }

    public void withdrawMoney(@NonNull String accountNumber, @NonNull Long amount){
        AccountEntity accountEntity = findByNumber(accountNumber);
        if( accountEntity != null) {
            Long balance = accountEntity.getBalance() - amount;
            accountEntity.setBalance(balance);
        }
    }

    public void addMoney(@NonNull String accountNumber, @NonNull Long amount){
        AccountEntity accountEntity = findByNumber(accountNumber);
        if( accountEntity != null) {
            Long balance = accountEntity.getBalance() + amount;
            accountEntity.setBalance(balance);
        }
    }

}
