package com.example.service;

import com.example.entity.AccountEntity;
import com.example.entity.TransactionRecordEntity;
import lombok.NonNull;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Created by ThinkPad
 *
 * @since 22.05.2017.
 */
public interface AccountService {
    AccountEntity createAccount(@NonNull String accountNumber);
    AccountEntity findByNumber(@NonNull String accountNumber);
    TransactionRecordEntity transferMoney( String fromAccountNumber,
                                          @NonNull String toAccountNumber,
                                          @NonNull Long amount);
//    Page<TransactionRecordEntity> getAccountHistory(String accountNumber, Integer pageNumber);
    List<TransactionRecordEntity> getAccountHistory(String accountNumber);
    AccountEntity setBalance(@NonNull String accountNumber, @NonNull Long amount);
    void withdrawMoney(@NonNull String accountNumber, @NonNull Long amount);
    void addMoney(@NonNull String accountNumber, @NonNull Long amount);
    Iterable<AccountEntity> getAllAccountList();
    Iterable<AccountEntity> getAllAccountListByPage();
}
