package com.example.model;

/**
 * Created by ThinkPad
 *
 * @since 26.05.2017.
 */
public class TransferMoney {
    private String fromAccountNumber;
    private String toAccountNumber;
    private Long amount;

    public TransferMoney() {
    }

    public TransferMoney(String fromAccountNumber, String toAccountNumber, Long amount) {
        this.fromAccountNumber = fromAccountNumber;
        this.toAccountNumber = toAccountNumber;
        this.amount = amount;
    }

    public String getFromAccountNumber() {
        return fromAccountNumber;
    }

    public void setFromAccountNumber(String fromAccountNumber) {
        this.fromAccountNumber = fromAccountNumber;
    }

    public String getToAccountNumber() {
        return toAccountNumber;
    }

    public void setToAccountNumber(String toAccountNumber) {
        this.toAccountNumber = toAccountNumber;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }
}
