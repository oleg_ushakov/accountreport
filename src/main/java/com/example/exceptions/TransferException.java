package com.example.exceptions;

/**
 * Created by zealot on 05.12.2015.
 */
public class TransferException extends RuntimeException {
    private String message;

    public TransferException(String s) {
    message = s;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
