package com.example.exceptions;

/**
 * Created by zealot on 05.12.2015.
 */
public class EmptyValueException extends RuntimeException {
    private String message;

    public EmptyValueException(String s) {
    message = s;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
