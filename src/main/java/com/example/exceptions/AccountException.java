package com.example.exceptions;

/**
 * Created by zealot on 05.12.2015.
 */
public class AccountException extends RuntimeException {
    private String message;

    public AccountException(String s) {
    message = s;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
