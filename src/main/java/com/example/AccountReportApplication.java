package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by ThinkPad
 * @since 19.05.2017.
 */
@SpringBootApplication
public class AccountReportApplication {
    public static void main(String[] args) {
        SpringApplication.run(AccountReportApplication.class, args);
    }
}
