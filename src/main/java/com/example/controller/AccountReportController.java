package com.example.controller;

import com.example.entity.TransactionRecordEntity;
import com.example.exceptions.AccountException;
import com.example.exceptions.TransferException;
import com.example.model.TransferMoney;
import com.example.entity.AccountEntity;
import com.example.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by ThinkPad
 * @since 19.05.2017.
 */
//@RestController
@Controller
@RequestMapping("/")
public class AccountReportController {
    @Autowired
    AccountService accountService;

    @RequestMapping("/")
    public String index(HttpServletRequest request, Model model){
        System.out.println("IndexClaimController method");
        model.addAttribute("account_list", accountService.getAllAccountList());
        model.addAttribute("account", new AccountEntity());
        return "account_list";
    }
    @RequestMapping(value="/create_new", method = RequestMethod.POST)
    public String createNewAccount(@ModelAttribute AccountEntity account, Model model){
        model.addAttribute("account_new", accountService.createAccount(account.getAccounNumber()));
//        return "account_created";
        return "redirect:/";
    }

    @RequestMapping(value="/find_by_number")
    public String findAccountByNumber(@RequestParam(value="number") String accountNumber, Model model){
        AccountEntity accountEntity = accountService.findByNumber(accountNumber);
        if (null == accountEntity){
            throw new AccountException("Счёт " + accountNumber + " не найден");
        }
        model.addAttribute("account_entity", accountEntity);
        model.addAttribute("transferMoney", new TransferMoney());
        return "account_info";
    }
    @RequestMapping(value="/find_by_number/{number}", method=RequestMethod.GET)
    public String findAccountByNumberGet(@PathVariable(value="number") String accountNumber, Model model){
        AccountEntity accountEntity = accountService.findByNumber(accountNumber);
        if (null == accountEntity){
            throw new AccountException("Счёт " + accountNumber + " не найден");
        }
        model.addAttribute("account_entity", accountEntity);
        model.addAttribute("transferMoney", new TransferMoney());
        return "account_info";
    }

    @RequestMapping(value="/transfer_to", method = RequestMethod.POST)
    public String transferToAccount(@ModelAttribute TransferMoney transferMoney, Model model){
        model.addAttribute("transaction_result", accountService.transferMoney(
                transferMoney.getFromAccountNumber(),
                transferMoney.getToAccountNumber(),
                transferMoney.getAmount()
                )
        );
        return "redirect:/";
    }

    @RequestMapping("/show_history_by_number")
    public String findTransactionByAccountNumber(@RequestParam(value="number") String accountNumber, Model model) {
        model.addAttribute("account_history", accountService.getAccountHistory(accountNumber));
        model.addAttribute("account_entity", accountService.findByNumber(accountNumber));
        return "account_history";
    }

    @RequestMapping(value="/show_history_by_number/{number}", method=RequestMethod.GET)
    public String findTransactionByAccountNumberGet(@PathVariable(value="number") String accountNumber, Model model) {
//        Page<TransactionRecordEntity> page = accountService.getAccountHistory(accountNumber, pageNumber);
//        int current = page.getNumber() + 1;
//        int begin = Math.max(1, current - 5);
//        int end = Math.min(begin + 10, page.getTotalPages());
//        model.addAttribute("account_history", page);
//        model.addAttribute("beginIndex", begin);
//        model.addAttribute("endIndex", end);
//        model.addAttribute("currentIndex", current);

        model.addAttribute("account_history", accountService.getAccountHistory(accountNumber));
        model.addAttribute("account_entity", accountService.findByNumber(accountNumber));
        return "account_history";
    }

    @RequestMapping(value="/add_to", method = RequestMethod.POST)
    public String addToAccount(@ModelAttribute TransferMoney transferMoney, Model model){
        String accountNumber = transferMoney.getToAccountNumber();
        model.addAttribute("transaction_result",
                accountService.transferMoney("0", accountNumber, transferMoney.getAmount())
        );
        AccountEntity accountEntity = accountService.findByNumber(accountNumber);
        if (null == accountEntity){
            throw new AccountException("Счёт " + accountNumber + " не найден");
        }
        model.addAttribute("account_entity", accountEntity);
        model.addAttribute("transferMoney", new TransferMoney());
        return "account_info";
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception ex) {
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", ex);
        mav.addObject("url", req.getRequestURL());
        mav.setViewName("error_page");
        return mav;
    }
    @ExceptionHandler({TransferException.class})
    public String databaseError() {
        return "databaseError";
    }
}
